/** @type {import('next').NextConfig} */
const nextConfig = {
  // basePath: '/dashboard',
  async redirects() {
    return [
      {
        source: '/',
        destination: '/dashboard',
        permanent: true,
      },
    ]
  },
};

export default nextConfig;
