import { UserIcon } from '@heroicons/react/16/solid';
import { Avatar, Breadcrumb } from 'antd';
import Link from 'next/link';

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <main
      className='grid grid-cols-1 min-h-screen'
      style={{ gridTemplateRows: '56px 1fr' }}
    >
      <nav
        role='topbar'
        className='sticky top-0 h-[56px] z-[401] flex justify-between items-center min-w-full p-12 bg-white'
      >
        <Breadcrumb
          items={[
            {
              title: <Link href='/'>Home</Link>,
            },
            {
              title: 'An Application',
            },
          ]}
        />
        <div>
          <Avatar icon={<UserIcon />} />
        </div>
      </nav>
      {children}
    </main>
  );
}
